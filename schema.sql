CREATE TABLE IF NOT EXISTS `leaderboard` (
  `membershipId` varchar(50) NOT NULL,
  `displayName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `membershipType` tinyint(1) NOT NULL,
  `timePlayed` int(11) NOT NULL,
  `gameVersion` tinyint(1) NOT NULL,
  `updateTime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`membershipId`,`gameVersion`),
  KEY `timePlayed` (`timePlayed`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `percentiles` (
  `gameVersion` int(11) NOT NULL,
  `membershipType` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `timePlayed` int(11) NOT NULL,
  PRIMARY KEY (`gameVersion`,`membershipType`,`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;