<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\Middlewares;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class PageCacheMiddleware
{
    protected $enabled;
    protected $cache;

    public function __construct(ContainerInterface $container)
    {
        $this->enabled = (bool)$container->get('cache')['enablePage'];
        $this->cache = $container->get('pageCacheHandler');
    }

    public function __invoke(Request $request, Response $response, callable $next)
    {
        if ($this->enabled) {
            $key = str_replace('/', '_', $request->getUri()->getPath()) . sha1($request->getUri()->getQuery());
            $hasCache = $this->cache->has($key);

            if (!$hasCache) {
                /** @var ResponseInterface $response */
                $response = $next($request, $response);
                $this->cache->set($key, (string)$response->getBody());
                return $response;
            }

            $body = $response->getBody();
            $body->write($this->cache->get($key));
            $response = $response->withBody($body);

            return $response;
        }

        return $next($request, $response);
    }
}
