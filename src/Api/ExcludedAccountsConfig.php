<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api;

use WastedOnDestiny\Application\ExcludedAccountsConfigInterface;

class ExcludedAccountsConfig implements ExcludedAccountsConfigInterface
{
    /** @var string[] */
    private $exclusions;

    public function __construct(array $exclusions)
    {
        $this->exclusions = $exclusions;
    }

    /**
     * @return string[]
     */
    public function get(): array
    {
        return $this->exclusions;
    }
}
