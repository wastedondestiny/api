<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\RequestHandlers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use WastedOnDestiny\Api\ApiResponse;

class NotFoundRequestHandler
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, string $message = 'Not Found')
    {
        $apiResponse = (new ApiResponse())->get();
        $apiResponse->code = 404;
        $apiResponse->message = $message;
        $body = $response->getBody();
        $body->write(json_encode($apiResponse));
        return $response
            ->withStatus(404)
            ->withHeader('content-type', 'application/json')
            ->withBody($body);
    }
}
