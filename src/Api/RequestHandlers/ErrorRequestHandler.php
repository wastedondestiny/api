<?php
/**
 * @noinspection PhpUndefinedClassInspection
 *
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\RequestHandlers;

use Bugsnag\Client;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;
use WastedOnDestiny\Api\ApiResponse;

class ErrorRequestHandler
{
    /** @var Client */
    private $bugsnag;

    /** @var NotFoundRequestHandler */
    private $notFoundRequestHandler;

    /** @var string[] */
    private $notFoundExceptions;

    public function __construct(Client $bugsnag, NotFoundRequestHandler $notFoundRequestHandler, array $notFoundExceptions) {
        $this->bugsnag = $bugsnag;
        $this->notFoundRequestHandler = $notFoundRequestHandler;
        $this->notFoundExceptions = $notFoundExceptions;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, Throwable $error)
    {
        if (in_array(get_class($error), $this->notFoundExceptions, true)) {
            return $this->notFoundRequestHandler->__invoke($request, $response, $error->getMessage());
        }

        $this->bugsnag->notifyException($error);
        $apiResponse = (new ApiResponse())->get();
        $apiResponse->code = $error->getCode();
        $apiResponse->message = $error->getMessage();
        $body = $response->getBody();
        $body->write(json_encode($apiResponse));
        return $response
            ->withStatus(500)
            ->withHeader('content-type', 'application/json')
            ->withBody($body);
    }
}
