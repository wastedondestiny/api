<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\Controllers;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use WastedOnDestiny\Api\ApiResponse;
use WastedOnDestiny\Application\UseCases\SearchPlayerUseCaseInterface;

class AccountsController
{
    /** @var SearchPlayerUseCaseInterface */
    private $useCase;

    public function __construct(SearchPlayerUseCaseInterface $searchPlayerUseCase)
    {
        $this->useCase = $searchPlayerUseCase;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        $displayName = $request->getQueryParams()['displayName'] ?? null;
        $membershipType = $request->getQueryParams()['membershipType'] ?? null;
        $membershipId = $request->getQueryParams()['membershipId'] ?? null;

        if (empty($displayName) && (empty($membershipType) || empty($membershipId))) {
            throw new InvalidArgumentException('Usage: "displayName" (required), or "membershipId" (required) and "membershipType" (required)');
        }

        $body = $response->getBody();
        $body->write(json_encode((new ApiResponse())->get($this->useCase->execute((string)$displayName, (int)$membershipType, (string)$membershipId))));
        return $response->withBody($body);
    }
}
