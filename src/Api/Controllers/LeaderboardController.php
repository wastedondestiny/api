<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\Controllers;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use WastedOnDestiny\Api\ApiResponse;
use WastedOnDestiny\Application\UseCases\GetLeaderboardPageUseCaseInterface;

class LeaderboardController
{
    /** @var GetLeaderboardPageUseCaseInterface */
    private $useCase;

    public function __construct(GetLeaderboardPageUseCaseInterface $getLeaderboardPageUseCase)
    {
        $this->useCase = $getLeaderboardPageUseCase;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        $gameVersion = $request->getQueryParams()['gameVersion'] ?? null;
        $membershipType = $request->getQueryParams()['membershipType'] ?? null;
        $page = $request->getQueryParams()['page'] ?? null;

        if (empty($gameVersion) || empty($membershipType) || empty($page)) {
            throw new InvalidArgumentException('Usage: "gameVersion" (required), "membershipType" (required) and "page" (required)');
        }

        $body = $response->getBody();
        $body->write(json_encode((new ApiResponse())->get($this->useCase->execute((int)$gameVersion, (int)$membershipType, (int)$page))));
        return $response->withBody($body);
    }
}
