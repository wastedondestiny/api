<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\Controllers;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use WastedOnDestiny\Api\ApiResponse;
use WastedOnDestiny\Application\UseCases\AutocompleteSearchUseCaseInterface;

class SearchController
{
    /** @var AutocompleteSearchUseCaseInterface */
    private $useCase;

    public function __construct(AutocompleteSearchUseCaseInterface $autocompleteSearchUseCase)
    {
        $this->useCase = $autocompleteSearchUseCase;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        $displayName = $request->getQueryParams()['displayName'] ?? null;

        if (empty($displayName)) {
            throw new InvalidArgumentException('Usage: "displayName" (required)');
        }

        $body = $response->getBody();
        $body->write(json_encode((new ApiResponse())->get($this->useCase->execute((string)$displayName))));
        return $response->withBody($body);
    }
}
