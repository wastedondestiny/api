<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api\Controllers;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use WastedOnDestiny\Api\ApiResponse;
use WastedOnDestiny\Application\UseCases\GetActivityBreakdownUseCaseInterface;

class BreakdownController
{
    /** @var GetActivityBreakdownUseCaseInterface */
    private $useCase;

    public function __construct(GetActivityBreakdownUseCaseInterface $activityBreakdownUseCase)
    {
        $this->useCase = $activityBreakdownUseCase;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        $gameVersion = $request->getQueryParams()['gameVersion'] ?? null;
        $membershipType = $request->getQueryParams()['membershipType'] ?? null;
        $membershipId = $request->getQueryParams()['membershipId'] ?? null;
        $characterId = $request->getQueryParams()['characterId'] ?? null;
        $page = $request->getQueryParams()['page'] ?? null;

        if (empty($gameVersion) || empty($membershipType) || empty($membershipId)) {
            throw new InvalidArgumentException(
                'Usage: "gameVersion" (required), "membershipId" (required), "membershipType" (required), "characterId" (required) and "page" (required)'
            );
        }

        $body = $response->getBody();
        $body->write(json_encode((new ApiResponse())->get($this->useCase->execute(
            (int)$gameVersion,
            (int)$membershipType,
            (string)$membershipId,
            (string)$characterId,
            (int)$page
        ))));
        return $response->withBody($body);
    }
}
