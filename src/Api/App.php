<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Api;

use Bugsnag\Client;
use DI\Container;
use DI\ContainerBuilder;
use function DI\create;
use function DI\get;
use Medoo\Medoo;
use PDO;
use WastedOnDestiny\Application\Exceptions\PlayerNotFoundException;
use WastedOnDestiny\Application\UseCases\AutocompleteSearchUseCase;
use WastedOnDestiny\Application\UseCases\AutocompleteSearchUseCaseInterface;
use WastedOnDestiny\Application\UseCases\GetActivityBreakdownUseCase;
use WastedOnDestiny\Application\UseCases\GetActivityBreakdownUseCaseInterface;
use WastedOnDestiny\Application\UseCases\GetCharactersUseCase;
use WastedOnDestiny\Application\UseCases\GetCharactersUseCaseInterface;
use WastedOnDestiny\Application\UseCases\GetGlobalAlertsUseCase;
use WastedOnDestiny\Application\UseCases\GetGlobalAlertsUseCaseInterface;
use WastedOnDestiny\Application\UseCases\GetLastMonthActivityUseCase;
use WastedOnDestiny\Application\UseCases\GetLastMonthActivityUseCaseInterface;
use WastedOnDestiny\Application\UseCases\GetLeaderboardPageUseCase;
use WastedOnDestiny\Application\UseCases\GetLeaderboardPageUseCaseInterface;
use WastedOnDestiny\Application\UseCases\GetLeaderboardRankUseCase;
use WastedOnDestiny\Application\UseCases\GetLeaderboardRankUseCaseInterface;
use WastedOnDestiny\Application\UseCases\SearchPlayerUseCase;
use WastedOnDestiny\Application\UseCases\SearchPlayerUseCaseInterface;
use WastedOnDestiny\Application\ExcludedAccountsConfigInterface;
use WastedOnDestiny\Business;
use WastedOnDestiny\Business\Cache\InMemoryCacheStorage;
use WastedOnDestiny\Business\Models\SeasonOfHuntProgressions;
use WastedOnDestiny\Business\Models\SeasonProgressions;
use WastedOnDestiny\DataAccess;
use WastedOnDestiny\DataAccess\BungieNetPlatformContext;
use WastedOnDestiny\DataAccess\BungieNetPlatformContextInterface;
use WastedOnDestiny\DataAccess\DatabaseContext;
use WastedOnDestiny\DataAccess\DatabaseContextInterface;
use WastedOnDestiny\DataAccess\FileContext;
use WastedOnDestiny\DataAccess\FileContextInterface;
use WastedOnDestiny\DataAccess\GuzzleContext;
use WastedOnDestiny\DataAccess\GuzzleContextInterface;

class App extends \DI\Bridge\Slim\App
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configureContainer(ContainerBuilder $builder): void
    {
        $notFoundExceptions = [
            PlayerNotFoundException::class
        ];

        $exceptionHandler = static function (Container $container) use ($notFoundExceptions) {
            return new RequestHandlers\ErrorRequestHandler($container->get('bugsnag'), $container->get('notFoundHandler'), $notFoundExceptions);
        };

        $handlers = [
            'notFoundHandler' => create(RequestHandlers\NotFoundRequestHandler::class),
            'notAllowedHandler' => create(RequestHandlers\NotAllowedRequestHandler::class),
            'errorHandler' => $exceptionHandler,
            'phpErrorHandler' => $exceptionHandler
        ];

        $dependencies = [
            'bugsnag' => static function (Container $container) {
                $commitHash = trim(exec('git log --pretty="%h" -n1 HEAD'));
                $bugsnag = Client::make((string)$container->get('bugsnagApiKey'));
                $bugsnag->setAppVersion((string)$commitHash);
                return $bugsnag;
            },
            Business\Cache\CacheStorageInterface::class => static function (Container $container) {
                $cache = $container->get('cache');

                if (!empty($cache) && array_key_exists('provider', $cache)) {
                    return $container->make($cache['provider'], [
                        'uri' => $cache['uri'],
                        'ttl' => $cache['ttl'] ?: 14400,
                    ]);
                }

                return new InMemoryCacheStorage();
            },
            BungieNetPlatformContextInterface::class => static function (Container $container) {
                $cache = $container->get('internalCache');
                $cacheManager = new InMemoryCacheStorage();

                if (!empty($cache) && array_key_exists('provider', $cache)) {
                    $cacheManager = $container->make($cache['provider'], [
                        'uri' => $cache['uri'],
                        'ttl' => $cache['ttl'] ?: 14400,
                    ]);
                }

                return new BungieNetPlatformContext(
                    $cacheManager,
                    (string)$container->get('apiKey')
                );
            },
            DatabaseContextInterface::class => static function (Container $container) {
                $config = $container->get('database');

                $database = new Medoo([
                    'database_type' => $config['type'],
                    'database_name' => $config['name'],
                    'server'        => $config['host'],
                    'username'      => $config['user'],
                    'password'      => $config['password'],
                    'charset'       => $config['charset'],
                    'port'          => $config['port'],
                    'option'        => [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
                ]);

                return new DatabaseContext($database);
            },
            FileContextInterface::class =>
                get(FileContext::class),
            GuzzleContextInterface::class =>
                get(GuzzleContext::class),

            ExcludedAccountsConfigInterface::class => static function (Container $container) {
                return new ExcludedAccountsConfig($container->get('exclusions'));
            },

            GetGlobalAlertsUseCaseInterface::class =>
                get(GetGlobalAlertsUseCase::class),
            GetCharactersUseCaseInterface::class =>
                get(GetCharactersUseCase::class),
            SearchPlayerUseCaseInterface::class =>
                get(SearchPlayerUseCase::class),
            GetLastMonthActivityUseCaseInterface::class =>
                get(GetLastMonthActivityUseCase::class),
            GetLeaderboardPageUseCaseInterface::class =>
                get(GetLeaderboardPageUseCase::class),
            GetLeaderboardRankUseCaseInterface::class =>
                get(GetLeaderboardRankUseCase::class),
            GetActivityBreakdownUseCaseInterface::class =>
                get(GetActivityBreakdownUseCase::class),
            AutocompleteSearchUseCaseInterface::class =>
                get(AutocompleteSearchUseCase::class),

            Business\Repositories\Core\GlobalAlertsRepositoryInterface::class =>
                get(DataAccess\Repositories\Core\GlobalAlertsRepository::class),
            Business\Repositories\Destiny\AccountHistoricalStatsRepositoryInterface::class =>
                get(DataAccess\Repositories\Destiny\AccountHistoricalStatsRepository::class),
            Business\Repositories\Destiny\AccountRepositoryInterface::class =>
                get(DataAccess\Repositories\Destiny\AccountRepository::class),
            Business\Repositories\Destiny\DestinyPlayerRepositoryInterface::class =>
                get(DataAccess\Repositories\Destiny\DestinyPlayerRepository::class),
            Business\Repositories\Destiny2\AccountHistoricalStatsRepositoryInterface::class =>
                get(DataAccess\Repositories\Destiny2\AccountHistoricalStatsRepository::class),
            Business\Repositories\Destiny2\ActivityHistoryRepositoryInterface::class =>
                get(DataAccess\Repositories\Destiny2\ActivityHistoryRepository::class),
            Business\Repositories\Destiny2\SearchDestinyPlayerByBungieNameRepositoryInterface::class =>
                get(DataAccess\Repositories\Destiny2\SearchDestinyPlayerByBungieNameRepository::class),
            Business\Repositories\Destiny2\HistoricalStatsRepositoryInterface::class =>
                get(DataAccess\Repositories\Destiny2\HistoricalStatsRepository::class),
            Business\Repositories\Destiny2\ProfileRepositoryInterface::class =>
                get(DataAccess\Repositories\Destiny2\ProfileRepository::class),
            Business\Repositories\User\MembershipRepositoryInterface::class =>
                get(DataAccess\Repositories\User\MembershipRepository::class),
            Business\Repositories\User\MembershipHardLinkRepositoryInterface::class =>
                get(DataAccess\Repositories\User\MembershipHardLinkRepository::class),
            Business\Repositories\Leaderboard\LeaderboardRepositoryInterface::class =>
                get(DataAccess\Repositories\Leaderboard\LeaderboardRepository::class),
            Business\Repositories\Leaderboard\PercentileRepositoryInterface::class =>
                get(DataAccess\Repositories\Leaderboard\PercentileRepository::class),
            Business\Repositories\Manifest\ReducedActivitiesRepositoryInterface::class =>
                get(DataAccess\Repositories\Manifest\ReducedActivitiesRepository::class),
            Business\Repositories\Manifest\SealsRepositoryInterface::class =>
                get(DataAccess\Repositories\Manifest\SealsRepository::class),
            Business\Repositories\Players\SearchRepositoryInterface::class =>
                get(DataAccess\Repositories\Players\SearchRepository::class),
            Business\Repositories\Manifest\SeasonsRepositoryInterface::class =>
                get(DataAccess\Repositories\Manifest\SeasonsRepository::class),
            Business\Repositories\User\SearchPrefixRepositoryInterface::class =>
                get(DataAccess\Repositories\User\SearchPrefixRepository::class)
        ];

        $builder->addDefinitions($this->config, $handlers, $dependencies);
    }
}
