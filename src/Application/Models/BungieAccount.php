<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\Models;

use DateTime;

class BungieAccount
{
    /** @var string */
    public $membershipId;

    /** @var string */
    public $displayName;

    /** @var DateTime */
    public $firstAccess;
}