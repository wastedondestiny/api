<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\Models;

class Breakdown
{
    /** @var int */
    public $story;

    /** @var int */
    public $strikes;

    /** @var int */
    public $raid;

    /** @var int */
    public $gambit;

    /** @var int */
    public $crucible;

    /** @var int */
    public $total;

    /** @var bool */
    public $empty;
}
