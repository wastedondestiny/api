<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\Models;

class LeaderboardPage
{
    /** @var LeaderboardEntry[] */
    public $players;

    /** @var int */
    public $page;

    /** @var int */
    public $count;

    /** @var int */
    public $totalPlayers;

    /** @var int */
    public $totalPlayersPlatform;
}