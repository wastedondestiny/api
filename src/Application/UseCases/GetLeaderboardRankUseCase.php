<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\UseCases;

use WastedOnDestiny\Application\ModelFactory;
use WastedOnDestiny\Application\Models\LeaderboardRank;
use WastedOnDestiny\Business\Models\LeaderboardEntry;
use WastedOnDestiny\Business\Repositories\Leaderboard\LeaderboardRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Leaderboard\PercentileRepositoryInterface;

class GetLeaderboardRankUseCase implements GetLeaderboardRankUseCaseInterface
{
    /** @var LeaderboardRepositoryInterface */
    private $leaderboardRepository;
    /** @var PercentileRepositoryInterface */
    private $percentileRepository;
    /** @var ModelFactory */
    private $factory;

    public function __construct(
        LeaderboardRepositoryInterface $leaderboardRepository,
        PercentileRepositoryInterface $percentileRepository)
    {
        $this->leaderboardRepository = $leaderboardRepository;
        $this->percentileRepository = $percentileRepository;
        $this->factory = new ModelFactory();
    }

    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param string $membershipId
     * @return LeaderboardRank
     */
    public function execute(int $gameVersion, int $membershipType, string $membershipId): LeaderboardRank
    {
        $topTenResult = $this->leaderboardRepository->get($gameVersion, $membershipType, 1);
        $topTenPosition = array_filter($topTenResult, static function (LeaderboardEntry $entry) use ($membershipId) {
            return $entry->membershipId === $membershipId;
        });

        if ($topTenPosition !== null && \count($topTenPosition) > 0) {
            return $this->factory->getLeaderboardRank(key($topTenPosition) + 1, true);
        }

        $player = $this->leaderboardRepository->getSingle($gameVersion, $membershipType, $membershipId);

        foreach ($this->percentileRepository->get($gameVersion, $membershipType) as $percentile) {
            if (isset($player) && $percentile->timePlayed <= $player->timePlayed) {
                return $this->factory->getLeaderboardRank($percentile->rank);
            }
        }

        return $this->factory->getLeaderboardRank(0);
    }
}
