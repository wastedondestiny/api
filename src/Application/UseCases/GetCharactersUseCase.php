<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\UseCases;

use WastedOnDestiny\Application\Exceptions\PlayerNotFoundException;
use WastedOnDestiny\Application\Exceptions\InvalidGameVersionException;
use WastedOnDestiny\Application\ModelFactory;
use WastedOnDestiny\Application\Models\Character;
use WastedOnDestiny\Business\Enums\DestinyGameVersion;
use WastedOnDestiny\Business\Models\CharacterStats;
use WastedOnDestiny\Business\Models\Destiny2CharacterData;
use WastedOnDestiny\Business\Models\DestinyCharacterData;
use WastedOnDestiny\Business\Models\Membership;
use WastedOnDestiny\Business\Repositories\Destiny\AccountHistoricalStatsRepositoryInterface as DestinyAccountHistoricalStatsRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Destiny\AccountRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Destiny2\AccountHistoricalStatsRepositoryInterface as Destiny2AccountHistoricalStatsRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Destiny2\ProfileRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Destiny2\DestinyPlayerRepository;
use WastedOnDestiny\Business\Repositories\Manifest\SealsRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Manifest\SeasonsRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Leaderboard\LeaderboardRepositoryInterface;
use WastedOnDestiny\Business\Repositories\User\MembershipRepositoryInterface;

class GetCharactersUseCase implements GetCharactersUseCaseInterface
{
    /** @var DestinyAccountHistoricalStatsRepositoryInterface */
    private $destinyAccountHistoricalStatsRepository;
    /** @var Destiny2AccountHistoricalStatsRepositoryInterface */
    private $destiny2AccountHistoricalStatsRepository;
    /** @var MembershipRepositoryInterface */
    private $membershipRepository;
    /** @var ProfileRepositoryInterface */
    private $profileRepository;
    /** @var AccountRepositoryInterface */
    private $accountRepository;
    /** @var SealsRepositoryInterface */
    private $sealsRepository;
    /** @var SeasonsRepositoryInterface */
    private $seasonsRepository;
    /** @var LeaderboardRepositoryInterface */
    private $leaderboardRepository;
    /** @var ModelFactory */
    private $factory;

    public function __construct(
        AccountRepositoryInterface $accountRepository,
        DestinyAccountHistoricalStatsRepositoryInterface $destinyAccountHistoricalStatsRepository,
        Destiny2AccountHistoricalStatsRepositoryInterface $destiny2AccountHistoricalStatsRepository,
        MembershipRepositoryInterface $membershipRepository,
        ProfileRepositoryInterface $profileRepository,
        SealsRepositoryInterface $sealsRepository,
        SeasonsRepositoryInterface $seasonsRepository,
        LeaderboardRepositoryInterface $leaderboardRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->destinyAccountHistoricalStatsRepository = $destinyAccountHistoricalStatsRepository;
        $this->destiny2AccountHistoricalStatsRepository = $destiny2AccountHistoricalStatsRepository;
        $this->membershipRepository = $membershipRepository;
        $this->profileRepository = $profileRepository;
        $this->sealsRepository = $sealsRepository;
        $this->seasonsRepository = $seasonsRepository;
        $this->leaderboardRepository = $leaderboardRepository;
        $this->factory = new ModelFactory();
    }

    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param string $membershipId
     * @return Character[]
     * @throws InvalidGameVersionException
     */
    public function execute(int $gameVersion, int $membershipType, string $membershipId): array
    {
        switch ($gameVersion) {
            case DestinyGameVersion::Destiny:
                $characters = $this->getDestinyCharacters($membershipType, $membershipId);
                break;
            case DestinyGameVersion::Destiny2:
                $characters = $this->getDestiny2Characters($membershipType, $membershipId);
                break;
            default:
                throw new InvalidGameVersionException(sprintf('Unknown game version: %s', $gameVersion));
        }

        if (empty($characters)) {
            throw new PlayerNotFoundException('Player has no characters.');
        }

        usort($characters, static function (Character $a, Character $b) {
            if ($a->deleted === $b->deleted) {
                return $b->timePlayed - $a->timePlayed;
            }

            return $a->deleted - $b->deleted;
        });

        return $characters;
    }

    /**
     * @param int $membershipType
     * @param string $membershipId
     * @return Character[]
     */
    private function getDestinyCharacters(int $membershipType, string $membershipId): array
    {
        $account = $this->accountRepository->get($membershipType, $membershipId);

        if (empty($account)) {
            return [];
        }

        $stats = $this->destinyAccountHistoricalStatsRepository->get($membershipType, $membershipId);
        $displayName = null;

        /** @var Membership $membership */
        foreach ($this->membershipRepository->get($membershipType, $membershipId) as $membership) {
            if ($membership->membershipId === $membershipId) {
                $displayName = $membership->displayName;
                break;
            }
        }

        $characters = [];
        $totalTimePlayed = 0;

        /** @var CharacterStats $characterStats */
        foreach ($stats as $characterStats) {
            /** @var DestinyCharacterData[] $characterData */
            $characterData = array_filter($account->characters, static function ($item) use ($characterStats) {
                return $characterStats->characterId === $item->characterId;
            });

            if (!empty($characterData)) {
                $characters[] = $this->factory->getDestinyCharacter($characterStats, reset($characterData));
            } else {
                $characters[] = $this->factory->getDestinyCharacter($characterStats, null);
            }

            $totalTimePlayed += $characterStats->timePlayed;
        }

        $this->saveInDatabase(DestinyGameVersion::Destiny, $membershipType, $membershipId, $totalTimePlayed, $displayName);

        return $characters;
    }

    /**
     * @param int $membershipType
     * @param string $membershipId
     * @return Character[]
     */
    private function getDestiny2Characters(int $membershipType, string $membershipId): array
    {
        $seasonPasses = $this->seasonsRepository->get();
        $profile = $this->profileRepository->get($membershipType, $membershipId, $seasonPasses);

        if (empty($profile)) {
            return [];
        }

        $stats = $this->destiny2AccountHistoricalStatsRepository->get($membershipType, $membershipId);
        $seals = $this->sealsRepository->get();
        $displayName = $profile->displayName;
        $characters = [];
        $totalTimePlayed = 0;

        /** @var CharacterStats $characterStats */
        foreach ($stats as $characterStats) {
            /** @var Destiny2CharacterData[] $characterData */
            $characterData = array_filter($profile->characters, static function ($item) use ($characterStats) {
                return $characterStats->characterId === $item->characterId;
            });

            if (!empty($characterData)) {
                $characters[] = $this->factory->getDestiny2Character($characterStats, $seals, reset($characterData));
            } else {
                $characters[] = $this->factory->getDestiny2Character($characterStats, $seals, null);
            }

            $totalTimePlayed += $characterStats->timePlayed;
        }

        $this->saveInDatabase(DestinyGameVersion::Destiny2, $membershipType, $membershipId, $totalTimePlayed, $displayName);
        return $characters;
    }

    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param string $membershipId
     * @param int $timePlayed
     * @param string $displayName
     */
    private function saveInDatabase(int $gameVersion, int $membershipType, string $membershipId, int $timePlayed, string $displayName): void
    {
        $this->leaderboardRepository->upsert($gameVersion, $membershipType, $membershipId, $timePlayed, $displayName);
    }
}
