<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\UseCases;

use WastedOnDestiny\Application\Exceptions\InvalidGameVersionException;
use WastedOnDestiny\Application\ModelFactory;
use WastedOnDestiny\Application\Models\Breakdown;
use WastedOnDestiny\Business\ActivityTypeChecker;
use WastedOnDestiny\Business\Enums\DestinyGameVersion;
use WastedOnDestiny\Business\Models\Activity;
use WastedOnDestiny\Business\Models\ActivityBreakdown;
use WastedOnDestiny\Business\Repositories\Destiny2\ActivityHistoryRepositoryInterface;

class GetActivityBreakdownUseCase implements GetActivityBreakdownUseCaseInterface
{
    /** @var ActivityHistoryRepositoryInterface */
    private $activityHistoryRepository;
    /** @var ModelFactory */
    private $factory;
    /** @var ActivityTypeChecker */
    private $activityTypeChecker;

    public function __construct(ActivityHistoryRepositoryInterface $activityHistoryRepository)
    {
        $this->activityHistoryRepository = $activityHistoryRepository;
        $this->factory = new ModelFactory();
        $this->activityTypeChecker = new ActivityTypeChecker();
    }

    /**
     * @param int $gameVersion
     * @param int $membershipType
     * @param string $membershipId
     * @param string $characterId
     * @param int $page
     * @return Breakdown
     * @throws InvalidGameVersionException
     */
    public function execute(int $gameVersion, int $membershipType, string $membershipId, string $characterId, int $page): Breakdown
    {
        switch ($gameVersion) {
            case DestinyGameVersion::Destiny:
                throw new InvalidGameVersionException('This action only supports Destiny 2 accounts');
                break;
            case DestinyGameVersion::Destiny2:
                return $this->factory->getActivityBreakdown($this->getDestiny2Breakdown($membershipType, $membershipId, $characterId, $page));
            default:
                throw new InvalidGameVersionException(sprintf('Unknown game version: %s', $gameVersion));
        }
    }

    /**
     * @param int $membershipType
     * @param string $membershipId
     * @param string $characterId
     * @param int $page
     * @return ActivityBreakdown
     */
    public function getDestiny2Breakdown(int $membershipType, string $membershipId, string $characterId, int $page): ActivityBreakdown
    {
        $breakdown = new ActivityBreakdown();
        $activities = $this->activityHistoryRepository->get($membershipType, $membershipId, $characterId, $page);

        if ($activities !== null && \count($activities)) {
            /** @var Activity $activity */
            foreach ($activities as $activity) {
                if ($this->activityTypeChecker->isStory($activity->mode)) {
                    $breakdown->story += $activity->timePlayed;
                    continue;
                }

                if ($this->activityTypeChecker->isStrike($activity->mode)) {
                    $breakdown->strike += $activity->timePlayed;
                    continue;
                }

                if ($this->activityTypeChecker->isRaid($activity->mode)) {
                    $breakdown->raid += $activity->timePlayed;
                    continue;
                }

                if ($this->activityTypeChecker->isGambit($activity->mode)) {
                    $breakdown->gambit += $activity->timePlayed;
                    continue;
                }

                if ($this->activityTypeChecker->isCrucible($activity->mode)) {
                    $breakdown->crucible += $activity->timePlayed;
                }
            }
        }

        return $breakdown;
    }
}
