<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\UseCases;

use GuzzleHttp\Exception\ServerException;
use InvalidArgumentException;
use WastedOnDestiny\Application\Exceptions\PlayerNotFoundException;
use WastedOnDestiny\Application\ModelFactory;
use WastedOnDestiny\Application\Models\DestinyAccount;
use WastedOnDestiny\Application\ExcludedAccountsConfigInterface;
use WastedOnDestiny\Business\Models\DestinyPlayer;
use WastedOnDestiny\Business\Models\Membership;
use WastedOnDestiny\Business\Repositories\Destiny\AccountRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Destiny\DestinyPlayerRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Destiny2\ProfileRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Destiny2\SearchDestinyPlayerByBungieNameRepositoryInterface;
use WastedOnDestiny\Business\Repositories\Manifest\ReducedActivitiesRepositoryInterface;
use WastedOnDestiny\Business\Repositories\User\MembershipHardLinkRepositoryInterface;
use WastedOnDestiny\Business\Repositories\User\MembershipRepositoryInterface;
use WastedOnDestiny\Business\Repositories\User\SearchPrefixRepositoryInterface;

class SearchPlayerUseCase implements SearchPlayerUseCaseInterface
{
    /** @var ReducedActivitiesRepositoryInterface */
    private $reducedActivitiesRepository;
    /** @var DestinyPlayerRepositoryInterface */
    private $destinyPlayerRepository;
    /** @var SearchDestinyPlayerByBungieNameRepositoryInterface */
    private $searchDestinyPlayerByBungieNameRepository;
    /** @var SearchPrefixRepositoryInterface */
    private $searchPrefixRepository;
    /** @var MembershipRepositoryInterface */
    private $membershipRepository;
    /** @var MembershipHardLinkRepositoryInterface */
    private $membershipHardLinkRepository;
    /** @var ProfileRepositoryInterface */
    private $profileRepository;
    /** @var AccountRepositoryInterface */
    private $accountRepository;
    /** @var ExcludedAccountsConfigInterface */
    private $excludedAccountsConfig;
    /** @var ModelFactory */
    private $factory;

    public function __construct(
        AccountRepositoryInterface $accountRepository,
        DestinyPlayerRepositoryInterface $destinyPlayerRepository,
        SearchDestinyPlayerByBungieNameRepositoryInterface $searchDestinyPlayerByBungieNameRepository,
        SearchPrefixRepositoryInterface $searchPrefixRepository,
        MembershipRepositoryInterface $membershipRepository,
        MembershipHardLinkRepositoryInterface $membershipHardLinkRepository,
        ProfileRepositoryInterface $profileRepository,
        ReducedActivitiesRepositoryInterface $reducedActivitiesRepository,
        ExcludedAccountsConfigInterface $excludedAccountsConfig)
    {
        $this->accountRepository = $accountRepository;
        $this->destinyPlayerRepository = $destinyPlayerRepository;
        $this->searchDestinyPlayerByBungieNameRepository = $searchDestinyPlayerByBungieNameRepository;
        $this->searchPrefixRepository = $searchPrefixRepository;
        $this->membershipRepository = $membershipRepository;
        $this->membershipHardLinkRepository = $membershipHardLinkRepository;
        $this->profileRepository = $profileRepository;
        $this->reducedActivitiesRepository = $reducedActivitiesRepository;
        $this->excludedAccountsConfig = $excludedAccountsConfig;
        $this->factory = new ModelFactory();
    }

    /**
     * @param string $displayName
     * @param int $membershipType
     * @param string $membershipId
     * @return DestinyAccount[]
     * @throws PlayerNotFoundException
     */
    public function execute(string $displayName, int $membershipType, string $membershipId): array
    {
        $result = [];

        if (!empty($displayName)) {
            if (is_numeric($displayName)) {
                $membershipHardLink = $this->membershipHardLinkRepository->get($displayName);

                if (!empty($membershipHardLink)) {
                    $result = $this->getMemberships($membershipHardLink->membershipType, $membershipHardLink->membershipId);
                }
            }

            $destinyPlayers = array_merge(
                $this->searchPrefixRepository->get($displayName),
                $this->searchDestinyPlayerByBungieNameRepository->get($displayName),
                $this->destinyPlayerRepository->get(-1, $displayName)
            );

            $excludedAccounts = $this->excludedAccountsConfig->get();

            if (empty($destinyPlayers) && empty($result)) {
                throw new PlayerNotFoundException(sprintf('Player %s was not found.', $displayName));
            }

            $memberships = $this->getMemberships($destinyPlayers[0]->membershipType, $destinyPlayers[0]->membershipId);

            /** @var DestinyAccount $membership */
            foreach ($memberships as $membership) {
                if (in_array($membership->membershipId, $excludedAccounts)) {
                    // Skip this account
                    continue;
                }
                
                $exists = count(array_filter($result, static function (DestinyAccount $account) use ($membership) {
                    return $account->membershipId === $membership->membershipId
                        && $account->membershipType === $membership->membershipType
                        && $account->gameVersion === $membership->gameVersion;
                })) > 0;

                if (!$exists) {
                    $result[] = $membership;
                }
            }
        } elseif (!empty($membershipType) && !empty($membershipId)) {
            $result = $this->getMemberships($membershipType, $membershipId);
        }

        if (empty($result)) {
            throw new PlayerNotFoundException(sprintf('Player %s was not found.', $displayName));
        }

        return $result;
    }

    /**
     * @param int $membershipType
     * @param string $membershipId
     * @return DestinyAccount[]
     */
    private function getMemberships(int $membershipType, string $membershipId): array
    {
        $result = [];
        $validDestinyAccounts = [];
        $memberships = $this->membershipRepository->get($membershipType, $membershipId);
        $activityDefinitions = $this->reducedActivitiesRepository->get();
        $excludedAccounts = $this->excludedAccountsConfig->get();

        if (!empty($memberships)) {
            /** @var Membership $membership */
            foreach ($memberships as $membership) {
                if (in_array($membership->membershipId, $excludedAccounts)) {
                    // Skip this account
                    continue;
                }

                if (!array_key_exists($membership->membershipId, $validDestinyAccounts)) {
                    try {
                        $profile = $this->profileRepository->get($membership->membershipType, $membership->membershipId, []);

                        if (!empty($profile)) {
                            $validDestinyAccounts[$membership->membershipId] = true;
                            $result[] = $this->factory->getDestiny2Account($membership, $profile, $activityDefinitions);
                        }
                    } catch (InvalidArgumentException $exception) {
                        // Failover: a membership can be present even if there are no Destiny accounts linked.
                    } catch (ServerException $exception) {
                        // Failover: an error might have been introduced in the API.
                    }

                    try {
                        $account = $this->accountRepository->get($membership->membershipType, $membership->membershipId);
                        
                        if (!empty($account)) {
                            $validDestinyAccounts[$membership->membershipId] = true;
                            $result[] = $this->factory->getDestinyAccount($membership, $account);
                        }
                    } catch (InvalidArgumentException $exception) {
                        // Failover: a membership can be present even if there are no Destiny accounts linked.
                        // Failover: a Destiny account may have only played on legacy consoles.
                    } catch (ServerException $exception) {
                        // Failover: the legacy API might not be available anymore, or an error might have been introduced.
                    }
                }
            }
        }

        return $result;
    }
}
