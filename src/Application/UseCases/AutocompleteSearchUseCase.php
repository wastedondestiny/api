<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Application\UseCases;

use WastedOnDestiny\Application\ModelFactory;
use WastedOnDestiny\Business\Repositories\Players\SearchRepositoryInterface;

class AutocompleteSearchUseCase implements AutocompleteSearchUseCaseInterface
{
    /** @var SearchRepositoryInterface */
    private $searchRepository;
    /** @var ModelFactory */
    private $factory;

    public function __construct(SearchRepositoryInterface $searchRepository)
    {
        $this->searchRepository = $searchRepository;
        $this->factory = new ModelFactory();
    }

    /**
     * @param string $displayName
     * @return array
     */
    public function execute(string $displayName): array
    {
        $players = [];

        if (!empty($displayName)) {
            $results = $this->searchRepository->get(0, $displayName);

            foreach ($results as $result) {
                $players[] = $this->factory->getAutocompletePlayer($result);
            }
        }

        return $players;
    }
}
