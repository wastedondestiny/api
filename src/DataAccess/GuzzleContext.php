<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use function GuzzleHttp\Psr7\build_query;
use GuzzleHttp\Psr7\Request;

class GuzzleContext implements GuzzleContextInterface
{
    /** @var Client */
    private $client;

    /** @var array */
    private $headers;

    public function __construct($headers = [])
    {
        $this->client = new Client();
        $this->headers = $headers;
    }

    /**
     * @param $method
     * @param $url
     * @param $parameters
     * @return string
     * @throws GuzzleException
     */
    public function call(string $method, string $url, array $parameters = []): string
    {
        try {
            if ($method === 'GET' || $method === 'DELETE') {
                $request = new Request($method, "{$url}?" . build_query($parameters), $this->headers);
            } else {
                $request = new Request($method, $url, $this->headers, json_encode($parameters));
            }

            $response = $this->client->send($request);
            $content = $response->getBody()->getContents();

            if (!empty($content)) {
                return $content;
            }
        } catch (Exception $exception) {
        }

        return '';
    }
}
