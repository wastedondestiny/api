<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Models;

class BungieNetPlatformResponse
{
    /** @var mixed|null */
    public $response;
    
    /** @var int */
    public $errorCode = 0;
    
    /** @var int */
    public $throttleSeconds = 0;
    
    /** @var string */
    public $errorStatus = '';
    
    /** @var string */
    public $message = '';
    
    /** @var mixed|null */
    public $messageData;
}