<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess;

class Utils
{
    public static function isNullOrEmpty($property): bool
    {
        return isset($property) && empty($property);
    }

    public static function isString($property): bool
    {
        return isset($property) && is_string($property);
    }

    public static function isFloat($property): bool
    {
        return isset($property) && is_float($property);
    }

    public static function isInteger($property): bool
    {
        return isset($property) && is_int($property);
    }

    public static function isBoolean($property): bool
    {
        return isset($property) && is_bool($property);
    }

    public static function isArray($property): bool
    {
        return isset($property) && is_array($property);
    }
}
