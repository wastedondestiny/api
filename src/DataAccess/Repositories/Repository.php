<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories;

use WastedOnDestiny\DataAccess\BusinessFactory;

abstract class Repository
{
    private $factory;

    public function __construct() {
        $this->factory = new BusinessFactory();
    }

    /**
     * @return BusinessFactory
     */
    public function getFactory(): BusinessFactory {
        return $this->factory;
    }
}