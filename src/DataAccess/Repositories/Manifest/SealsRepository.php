<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Manifest;

use WastedOnDestiny\Business\Repositories\Manifest\SealsRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\ManifestRepository;

final class SealsRepository extends ManifestRepository implements SealsRepositoryInterface
{
    /**
     * @return array
     */
    public function get(): array
    {
        return $this->getContext()->make('./manifest/records.json');
    }
}
