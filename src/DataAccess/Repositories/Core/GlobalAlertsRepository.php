<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Core;

use WastedOnDestiny\Business\Models\GlobalAlert;
use WastedOnDestiny\Business\Repositories\Core\GlobalAlertsRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class GlobalAlertsRepository extends BungieNetPlatformRepository implements GlobalAlertsRepositoryInterface
{
    /**
     * @return GlobalAlert[]
     */
    public function get(): array
    {
        $result = $this->getContext()->make(
            'GET',
            self::BASE_URL . '/Platform/GlobalAlerts/'
        );

        return $this->getFactory()->getGlobalAlerts($result);
    }
}
