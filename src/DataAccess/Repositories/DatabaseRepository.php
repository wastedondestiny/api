<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories;

use WastedOnDestiny\DataAccess\DatabaseContextInterface;

abstract class DatabaseRepository extends Repository
{
    /** @var DatabaseContextInterface */
    private $context;

    public function __construct(DatabaseContextInterface $context) {
        parent::__construct();
        $this->context = $context;
    }

    /**
     * @return DatabaseContextInterface
     */
    public function getContext(): DatabaseContextInterface {
        return $this->context;
    }
}