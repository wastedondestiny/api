<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\User;

use WastedOnDestiny\Business\Models\Membership;
use WastedOnDestiny\Business\Repositories\User\SearchPrefixRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class SearchPrefixRepository extends BungieNetPlatformRepository implements SearchPrefixRepositoryInterface
{
    /**
     * @param string $displayName
     * @return Membership|null
     */
    public function get(string $displayName)
    {
        $response = $this->getContext()->make(
            'GET',
            vsprintf(
                self::BASE_URL . '/Platform/User/Search/Prefix/%s/0/',
                [ rawurlencode($displayName) ]
            )
        );

        if (!empty($response->message)) {
            return $this->getFactory()->getFirstSearchPlayer($response);
        } else {
            return null;
        }
    }
}
