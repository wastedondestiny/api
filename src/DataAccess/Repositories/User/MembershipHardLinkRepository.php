<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\User;

use WastedOnDestiny\Business\Models\Membership;
use WastedOnDestiny\Business\Repositories\User\MembershipHardLinkRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class MembershipHardLinkRepository extends BungieNetPlatformRepository implements MembershipHardLinkRepositoryInterface
{
    /**
     * @param string $steamId64
     * @return Membership|null
     */
    public function get(string $steamId64)
    {
        $response = $this->getContext()->make(
            'GET',
            vsprintf(
                self::BASE_URL . '/Platform/User/GetMembershipFromHardLinkedCredential/12/%s/',
                [ $steamId64 ]
            )
        );

        if (!empty($response->message)) {
            return $this->getFactory()->getMembership($response);
        } else {
            return null;
        }
    }
}
