<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Destiny;

use WastedOnDestiny\Business\Models\Account;
use WastedOnDestiny\Business\Repositories\Destiny\AccountRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class AccountRepository extends BungieNetPlatformRepository implements AccountRepositoryInterface
{
    /**
     * @param int $membershipType
     * @param string $membershipId
     * @return Account|null
     */
    public function get(int $membershipType, string $membershipId)
    {
        $result = $this->getContext()->make(
            'GET',
            vsprintf(
                self::BASE_URL . '/d1/Platform/Destiny/%s/Account/%s/',
                [ $membershipType, $membershipId ]
            )
        );

        if (!empty($result->response)) {
            return $this->getFactory()->getAccount($result);
        } else {
            return null;
        }
    }
}
