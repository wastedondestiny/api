<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Destiny;

use WastedOnDestiny\Business\Models\DestinyPlayer;
use WastedOnDestiny\Business\Repositories\Destiny\DestinyPlayerRepositoryInterface;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class DestinyPlayerRepository extends BungieNetPlatformRepository implements DestinyPlayerRepositoryInterface
{
    /**
     * @param int $membershipType
     * @param string $displayName
     * @return DestinyPlayer[]
     */
    public function get(int $membershipType, string $displayName): array
    {
        $result = $this->getContext()->make(
            'GET',
            vsprintf(
                self::BASE_URL . '/Platform/Destiny/SearchDestinyPlayer/%s/%s/',
                [ $membershipType, rawurlencode($displayName) ]
            )
        );

        return $this->getFactory()->getDestinyPlayers($result);
    }
}
