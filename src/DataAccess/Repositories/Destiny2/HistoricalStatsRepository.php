<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess\Repositories\Destiny2;

use WastedOnDestiny\Business\Repositories\Destiny2\HistoricalStatsRepositoryInterface;
use WastedOnDestiny\DataAccess\Models\BungieNetPlatformResponse;
use WastedOnDestiny\DataAccess\Repositories\BungieNetPlatformRepository;

final class HistoricalStatsRepository extends BungieNetPlatformRepository implements HistoricalStatsRepositoryInterface
{
    /**
     * @param int $membershipType
     * @param string $membershipId
     * @param string $characterId
     * @return BungieNetPlatformResponse
     */
    public function get(int $membershipType, string $membershipId, string $characterId): BungieNetPlatformResponse
    {
        return $this->getContext()->make(
            'GET',
            vsprintf(
                self::BASE_URL . '/Platform/Destiny2/%s/Account/%s/Character/%s/Stats/',
                [ $membershipType, $membershipId, $characterId ]
            )
        );
    }
}
