<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess;

use InvalidArgumentException;
use WastedOnDestiny\Business\Models\Account;
use WastedOnDestiny\Business\Models\Activity;
use WastedOnDestiny\Business\Models\BungieAccount;
use WastedOnDestiny\Business\Models\CharacterStats;
use WastedOnDestiny\Business\Models\Destiny2CharacterData;
use WastedOnDestiny\Business\Models\DestinyCharacterData;
use WastedOnDestiny\Business\Models\DestinyPlayer;
use WastedOnDestiny\Business\Models\ElasticPlayer;
use WastedOnDestiny\Business\Models\GlobalAlert;
use WastedOnDestiny\Business\Models\LeaderboardEntry;
use WastedOnDestiny\Business\Models\Membership;
use WastedOnDestiny\Business\Models\Percentile;
use WastedOnDestiny\Business\Models\Profile;
use WastedOnDestiny\Business\Models\SeasonPass;
use WastedOnDestiny\DataAccess\Models\BungieNetPlatformResponse;

class BusinessFactory
{
    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @return Membership[]
     */
    public function getFirstSearchPlayer(BungieNetPlatformResponse $bungieNetPlatformResponse): array
    {
        if (!@Utils::isArray($bungieNetPlatformResponse->response->searchResults)) {
            throw new InvalidArgumentException('"$bungieNetPlatformResponse->response->searchResults" must be an array');
        }

        if (@Utils::isNullOrEmpty($bungieNetPlatformResponse->response->searchResults[0])) {
            throw new InvalidArgumentException('"$bungieNetPlatformResponse->response->searchResults[0]" is empty');
        }

        $searchResult = $bungieNetPlatformResponse->response->searchResults[0];
        $result = [];

        foreach ($searchResult->destinyMemberships as $membershipObject) {
            if (!@Utils::isInteger($membershipObject->membershipType)) {
                throw new InvalidArgumentException('"$membershipObject->membershipType" must be an integer');
            }

            if (!@Utils::isString($membershipObject->membershipId)) {
                throw new InvalidArgumentException('"$membershipObject->membershipId" must be a string');
            }

            if (!@Utils::isString($membershipObject->displayName)) {
                throw new InvalidArgumentException('"$membershipObject->displayName" must be a string');
            }

            $membership = new Membership();
            $membership->membershipType = $membershipObject->membershipType;
            $membership->membershipId = $membershipObject->membershipId;
            $membership->displayName = $membershipObject->displayName;
            $result[] = $membership;
        }

        return $result;
    }

    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @return DestinyPlayer[]
     */
    public function getDestinyPlayers(BungieNetPlatformResponse $bungieNetPlatformResponse): array
    {
        if (!@Utils::isArray((array)$bungieNetPlatformResponse->response)) {
            throw new InvalidArgumentException('"$bungieNetPlatformResponse->response" must be an array');
        }

        $result = [];

        foreach ($bungieNetPlatformResponse->response as $destinyPlayerObject) {
            if (!@Utils::isInteger($destinyPlayerObject->membershipType)) {
                throw new InvalidArgumentException('"$destinyPlayerObject->membershipType" must be an integer');
            }

            if (!@Utils::isString($destinyPlayerObject->membershipId)) {
                throw new InvalidArgumentException('"$destinyPlayerObject->membershipId" must be a string');
            }

            $result[] = new DestinyPlayer($destinyPlayerObject->membershipType, $destinyPlayerObject->membershipId);
        }

        return $result;
    }

    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @return Membership[]
     */
    public function getMemberships(BungieNetPlatformResponse $bungieNetPlatformResponse): array
    {
        if (!@Utils::isArray($bungieNetPlatformResponse->response->destinyMemberships)) {
            throw new InvalidArgumentException('"$bungieNetPlatformResponse->response->destinyMemberships" must be an array');
        }

        $result = [];
        $bungieAccount = null;

        if (@Utils::isString($bungieNetPlatformResponse->response->bungieNetUser->displayName) &&
            @Utils::isString($bungieNetPlatformResponse->response->bungieNetUser->firstAccess) &&
            @Utils::isString($bungieNetPlatformResponse->response->bungieNetUser->membershipId)
        ) {
            $bungieAccount = new BungieAccount();
            $bungieAccount->displayName = $bungieNetPlatformResponse->response->bungieNetUser->displayName;
            $bungieAccount->firstAccess = $bungieNetPlatformResponse->response->bungieNetUser->firstAccess;
            $bungieAccount->membershipId = $bungieNetPlatformResponse->response->bungieNetUser->membershipId;
        }

        foreach ($bungieNetPlatformResponse->response->destinyMemberships as $membershipObject) {
            if (!@Utils::isInteger($membershipObject->membershipType)) {
                throw new InvalidArgumentException('"$membershipObject->membershipType" must be an integer');
            }

            if (!@Utils::isString($membershipObject->membershipId)) {
                throw new InvalidArgumentException('"$membershipObject->membershipId" must be a string');
            }

            if (!@Utils::isString($membershipObject->bungieGlobalDisplayName)) {
                throw new InvalidArgumentException('"$membershipObject->bungieGlobalDisplayName" must be a string');
            }

            if (!@Utils::isString($membershipObject->displayName)) {
                throw new InvalidArgumentException('"$membershipObject->displayName" must be a string');
            }

            $membership = new Membership();
            $membership->membershipType = $membershipObject->membershipType;
            $membership->membershipId = $membershipObject->membershipId;
            $membership->displayName = $membershipObject->displayName;
            $membership->globalDisplayName = $membershipObject->bungieGlobalDisplayName . '#' . $membershipObject->bungieGlobalDisplayNameCode;
            $membership->bungieAccount = $bungieAccount;
            $result[] = $membership;
        }

        return $result;
    }

    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @return Membership
     */
    public function getMembership(BungieNetPlatformResponse $bungieNetPlatformResponse): Membership
    {
        if (!@Utils::isInteger($bungieNetPlatformResponse->response->membershipType)) {
            throw new InvalidArgumentException('"$bungieNetPlatformResponse->response->membershipType" must be an integer');
        }

        if (!@Utils::isString($bungieNetPlatformResponse->response->membershipId)) {
            throw new InvalidArgumentException('"$bungieNetPlatformResponse->response->membershipId" must be a string');
        }

        $membership = new Membership();
        $membership->membershipType = $bungieNetPlatformResponse->response->membershipType;
        $membership->membershipId = $bungieNetPlatformResponse->response->membershipId;

        return $membership;
    }

    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @param array $seasonPasses
     * @return Profile
     */
    public function getProfile(BungieNetPlatformResponse $bungieNetPlatformResponse, array $seasonPasses): Profile
    {
        if (!@Utils::isString($bungieNetPlatformResponse->response->profile->data->userInfo->displayName)) {
            throw new InvalidArgumentException('"$bungieNetPlatformResponse->response->profile->data->userInfo->displayName" must be a string');
        }

        $profile = new Profile();
        $profile->dateLastPlayed = $bungieNetPlatformResponse->response->profile->data->dateLastPlayed ?? null;
        $profile->displayName = $bungieNetPlatformResponse->response->profile->data->userInfo->displayName;

        if ($bungieNetPlatformResponse->response->profile->data->userInfo->crossSaveOverride) {
            $profile->crossSaveActive = $bungieNetPlatformResponse->response->profile->data->userInfo->crossSaveOverride === $bungieNetPlatformResponse->response->profile->data->userInfo->membershipType;
        }

        if (
            isset($bungieNetPlatformResponse->response->profileRecords->data) &&
            @Utils::isInteger($bungieNetPlatformResponse->response->profileRecords->data->score)
        ) {
            $profile->triumph = $bungieNetPlatformResponse->response->profileRecords->data->score;
            $profile->legacyTriumph = $bungieNetPlatformResponse->response->profileRecords->data->legacyScore;
        } else {
            $profile->triumph = 0;
            $profile->legacyTriumph = 0;
        }

        $seasonPass = $this->getSeasonPass($seasonPasses, $bungieNetPlatformResponse->response->profile->data->currentSeasonHash ?? 0);
        $profile->characters = $this->getDestiny2CharacterData($bungieNetPlatformResponse, $seasonPass);

        if (isset($bungieNetPlatformResponse->response->characterActivities->data)) {
            $profile->characterActivities = (array)$bungieNetPlatformResponse->response->characterActivities->data;
        }

        $profile->versionOwned = $bungieNetPlatformResponse->response->profile->data->versionsOwned;
        $profile->seasonsOwned = $bungieNetPlatformResponse->response->profile->data->seasonHashes;

        return $profile;
    }

    /**
     * @param array $seasonPasses
     * @param int $currentSeasonHash
     * @return SeasonPass|null
     */
    public function getSeasonPass(array $seasonPasses, int $currentSeasonHash)
    {
        if (array_key_exists($currentSeasonHash, $seasonPasses)) {
            $seasonPass = new SeasonPass();
            $seasonPass->rewardProgressionHash = $seasonPasses[$currentSeasonHash]['rewardProgressionHash'];
            $seasonPass->prestigeProgressionHash = $seasonPasses[$currentSeasonHash]['prestigeProgressionHash'];
            return $seasonPass;
        }

        return null;
    }

    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @param SeasonPass|null $seasonPass
     * @return Destiny2CharacterData[]
     */
    public function getDestiny2CharacterData(BungieNetPlatformResponse $bungieNetPlatformResponse, $seasonPass): array
    {
        if (@Utils::isNullOrEmpty($bungieNetPlatformResponse->response->characters->data)) {
            throw new InvalidArgumentException('$bungieNetPlatformResponse->response->characters->data cannot be null or empty');
        }

        $characters = [];

        foreach ($bungieNetPlatformResponse->response->characters->data as $character) {
            if (!@Utils::isString($character->characterId) ||
                !@Utils::isInteger($character->classType) ||
                !@Utils::isInteger($character->genderType) ||
                !@Utils::isInteger($character->light) ||
                !@Utils::isInteger($character->raceType)
            ) {
                throw new InvalidArgumentException('Character data cannot be null');
            }

            $characterData = new Destiny2CharacterData();
            $characterData->backgroundPath = $character->emblemBackgroundPath ?? '';
            $characterData->characterId = $character->characterId;
            $characterData->emblemPath = $character->emblemPath ?? '/img/misc/missing_icon_d2.png';
            $characterData->power = $character->light;
            $characterData->title = $character->titleRecordHash ?? '';
            $characterData->timeAfk = $character->minutesPlayedTotal * 60;

            if (
                isset($bungieNetPlatformResponse->response->characterProgressions->data) &&
                isset($bungieNetPlatformResponse->response->profileProgression->data) &&
                property_exists($bungieNetPlatformResponse->response->characterProgressions->data, $characterData->characterId) &&
                $seasonPass !== null
            ) {
                $globalProgressions = $bungieNetPlatformResponse->response->profileProgression->data;
                $progressions = $bungieNetPlatformResponse->response->characterProgressions->data->{$characterData->characterId}->progressions;
                $characterData->level = $progressions->{$seasonPass->rewardProgressionHash}->currentProgress > 0
                    ? $progressions->{$seasonPass->rewardProgressionHash}->level
                    : 1;
                $characterData->level += $progressions->{$seasonPass->prestigeProgressionHash}->currentProgress > 0
                    ? $progressions->{$seasonPass->prestigeProgressionHash}->level
                    : 0;
                $characterData->legend = $globalProgressions->seasonalArtifact->powerBonus;
            } else {
                $characterData->level = 1;
                $characterData->legend = 0;
            }

            switch ($character->classType) {
                case 0: $characterData->charClass = 'Titan'; break;
                case 1: $characterData->charClass = 'Hunter'; break;
                case 2: $characterData->charClass = 'Warlock'; break;
                default:
                    throw new InvalidArgumentException(sprintf('Invalid character classType: %s', $character->classType));
            }

            switch ($character->raceType) {
                case 0: $characterData->race = 'Human'; break;
                case 1: $characterData->race = 'Awoken'; break;
                case 2: $characterData->race = 'Exo'; break;
                default:
                    throw new InvalidArgumentException(sprintf('Invalid character raceType: %s', $character->raceType));
            }

            switch ($character->genderType) {
                case 0: $characterData->gender = 'Male'; break;
                case 1: $characterData->gender = 'Female'; break;
                default:
                    throw new InvalidArgumentException(sprintf('Invalid character genderType: %s', $character->genderType));
            }

            $characters[] = $characterData;
        }

        return $characters;
    }

    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @return Account
     */
    public function getAccount(BungieNetPlatformResponse $bungieNetPlatformResponse): Account
    {
        $account = new Account();
        $account->dateLastPlayed = $bungieNetPlatformResponse->response->data->dateLastPlayed ?? null;

        if (@Utils::isInteger($bungieNetPlatformResponse->response->data->grimoireScore)) {
            $account->grimoire = $bungieNetPlatformResponse->response->data->grimoireScore;
        } else {
            $account->grimoire = 0;
        }

        $account->characters = $this->getDestinyCharacterData($bungieNetPlatformResponse);

        return $account;
    }

    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @return DestinyCharacterData[]
     */
    public function getDestinyCharacterData(BungieNetPlatformResponse $bungieNetPlatformResponse): array
    {
        if (!@Utils::isArray($bungieNetPlatformResponse->response->data->characters) || empty($bungieNetPlatformResponse->response->data->characters)) {
            throw new InvalidArgumentException('$bungieNetPlatformResponse->response->data->characters must not be empty');
        }

        $characters = [];

        foreach ($bungieNetPlatformResponse->response->data->characters as $character) {
            if (!@Utils::isString($character->characterBase->characterId) ||
                !@Utils::isInteger($character->characterBase->classType) ||
                !@Utils::isInteger($character->characterBase->genderType) ||
                !@Utils::isInteger($character->characterBase->powerLevel) ||
                !@Utils::isInteger($character->characterBase->raceHash)
            ) {
                throw new InvalidArgumentException('Character data cannot be null');
            }

            $characterData = new DestinyCharacterData();
            $characterData->backgroundPath = $character->backgroundPath ?? '';
            $characterData->characterId = $character->characterBase->characterId;
            $characterData->emblemPath = $character->emblemPath ?? '/img/misc/missing_icon_d2.png';
            $characterData->light = $character->characterBase->powerLevel;
            $characterData->level = $character->characterLevel;

            switch ($character->characterBase->classType) {
                case 0: $characterData->charClass = 'Titan'; break;
                case 1: $characterData->charClass = 'Hunter'; break;
                case 2: $characterData->charClass = 'Warlock'; break;
                default:
                    throw new InvalidArgumentException(sprintf('Invalid character classType: %s', $character->characterBase->classType));
            }

            switch ((string)$character->characterBase->raceHash) {
                case '3887404748': $characterData->race = 'Human'; break;
                case '2803282938': $characterData->race = 'Awoken'; break;
                case '898834093': $characterData->race = 'Exo'; break;
                default:
                    throw new InvalidArgumentException(sprintf('Invalid character raceHash: %s', $character->characterBase->raceHash));
            }

            switch ($character->characterBase->genderType) {
                case 0: $characterData->gender = 'Male'; break;
                case 1: $characterData->gender = 'Female'; break;
                default:
                    throw new InvalidArgumentException(sprintf('Invalid character genderType: %s', $character->characterBase->genderType));
            }

            $characters[] = $characterData;
        }

        return $characters;
    }

    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @return CharacterStats[]
     */
    public function getCharacterStats(BungieNetPlatformResponse $bungieNetPlatformResponse): array
    {
        if (!@Utils::isArray($bungieNetPlatformResponse->response->characters)) {
            throw new InvalidArgumentException('$bungieNetPlatformResponse->response->characters must be an array');
        }

        $characters = [];

        foreach ($bungieNetPlatformResponse->response->characters as $character) {
            if (!@Utils::isString($character->characterId) ||
                !@Utils::isBoolean($character->deleted)
            ) {
                throw new InvalidArgumentException('$character must be an object');
            }

            $characterStats = new CharacterStats();
            $characterStats->characterId = $character->characterId;
            $characterStats->deleted = $character->deleted;

            if (@Utils::isFloat($character->merged->allTime->secondsPlayed->basic->value)) {
                $characterStats->timePlayed = (int)$character->merged->allTime->secondsPlayed->basic->value;
            } else {
                $characterStats->timePlayed = 0;
            }

            if (@Utils::isFloat($character->results->allPvP->allTime->killsDeathsAssists->basic->value)) {
                $characterStats->pvpKda = $character->results->allPvP->allTime->killsDeathsAssists->basic->value;
            } else {
                $characterStats->pvpKda = 0.0;
            }

            if (@Utils::isFloat($character->results->allPvE->allTime->killsDeathsAssists->basic->value)) {
                $characterStats->pveKda = $character->results->allPvE->allTime->killsDeathsAssists->basic->value;
            } else {
                $characterStats->pveKda = 0.0;
            }

            $characters[] = $characterStats;
        }

        return $characters;
    }

    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @return GlobalAlert[]
     */
    public function getGlobalAlerts(BungieNetPlatformResponse $bungieNetPlatformResponse): array
    {
        if (!@Utils::isArray($bungieNetPlatformResponse->response)) {
            throw new InvalidArgumentException('$bungieNetPlatformResponse->response must be an array');
        }

        $alerts = [];

        foreach ($bungieNetPlatformResponse->response as $alertItem) {
            $alert = new GlobalAlert();
            $alert->html = $alertItem->AlertHtml;
            $alert->timestamp = $alertItem->AlertTimestamp;
            $alert->link = $alertItem->AlertLink;
            $alert->level = $alertItem->AlertLevel;
            $alert->type = $alertItem->AlertType;
            $alerts[] = $alert;
        }

        return $alerts;
    }

    /**
     * @param BungieNetPlatformResponse $bungieNetPlatformResponse
     * @return Activity[]|null
     */
    public function getActivities(BungieNetPlatformResponse $bungieNetPlatformResponse): ?array
    {
        if (!@Utils::isArray($bungieNetPlatformResponse->response->activities)) {
            return null;
        }

        $activities = [];

        foreach ($bungieNetPlatformResponse->response->activities as $activityItem) {
            if (!@Utils::isFloat($activityItem->values->timePlayedSeconds->basic->value)) {
                throw new InvalidArgumentException('$activityItem->values->timePlayedSeconds->basic->value must be an integer');
            }

            if (!@Utils::isInteger($activityItem->activityDetails->mode)) {
                throw new InvalidArgumentException('$activityItem->activityDetails->mode must be an integer');
            }

            if (!@Utils::isString($activityItem->period)) {
                throw new InvalidArgumentException('$activityItem->period must be a string');
            }

            $activity = new Activity();
            $activity->mode = $activityItem->activityDetails->mode;
            $activity->period = $activityItem->period;
            $activity->timePlayed = (int)$activityItem->values->timePlayedSeconds->basic->value;
            $activities[] = $activity;
        }

        return $activities;
    }

    /**
     * @param array[] $entries
     * @return LeaderboardEntry[]
     */
    public function getLeaderboardEntries(array $entries): array
    {
        $leaderboardEntries = [];

        foreach ($entries as $entry) {
            if (!@Utils::isArray($entry)) {
                throw new InvalidArgumentException('$entry must be an array');
            }

            $leaderboardEntry = new LeaderboardEntry();
            $leaderboardEntry->gameVersion = (int)$entry['gameVersion'];
            $leaderboardEntry->membershipType = (int)$entry['membershipType'];
            $leaderboardEntry->timePlayed = (int)$entry['timePlayed'];
            $leaderboardEntry->membershipId = (string)$entry['membershipId'];
            $leaderboardEntry->displayName = (string)$entry['displayName'];
            $leaderboardEntries[] = $leaderboardEntry;
        }

        return $leaderboardEntries;
    }

    /**
     * @param array[] $entries
     * @return Percentile[]
     */
    public function getPercentiles(array $entries): array
    {
        $percentiles = [];

        foreach ($entries as $entry) {
            if (!@Utils::isArray($entry)) {
                throw new InvalidArgumentException('$entry must be an array');
            }

            $percentile = new Percentile();
            $percentile->rank = (int)$entry['rank'];
            $percentile->timePlayed = (int)$entry['timePlayed'];
            $percentiles[] = $percentile;
        }

        return $percentiles;
    }

    /**
     * @param array $entry
     * @return ElasticPlayer
     */
    public function getElasticPlayer(array $entry): ElasticPlayer
    {
        $player = new ElasticPlayer();

        $player->blizzardName = $entry['blizzardName'];
        $player->bnetId = $entry['bnetId'];
        $player->displayName = $entry['displayName'];
        $player->lastPlayed = $entry['lastPlayed'];
        $player->locale = $entry['locale'];
        $player->membershipId = $entry['membershipId'];
        $player->membershipType = $entry['membershipType'];

        return $player;
    }
}
