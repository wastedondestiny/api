<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess;

use Exception;
use Medoo\Medoo;
use RuntimeException;

/**
 * Class MedooDatabaseContext
 *
 * @package WastedOnDestiny\DataAccess
 */
class DatabaseContext implements DatabaseContextInterface
{
    /**
     * @var Medoo
     */
    private $database;

    public function __construct(Medoo $database) {
        $this->database = $database;
    }

    /**
     * Insert new rows in a database table
     *
     * @param string $table
     *          Table name
     * @param array  $data
     *          Values to insert
     */
    public function insert(string $table, array $data): void {
        try {
            $this->database->insert($table, $data);
        } catch (Exception $exception) {
            throw new RuntimeException(sprintf('Could not insert row(s) in table %s', $table), (int)$exception->getCode(), $exception);
        }
    }

    /**
     * Update rows from a database table
     *
     * @param string $table
     *          Table name
     * @param array  $data
     *          New values to insert
     * @param array  $where
     *          Update condition
     */
    public function update(string $table, array $data, array $where = []): void {
        try {
            $this->database->update($table, $data, $where);
        } catch (Exception $exception) {
            throw new RuntimeException(sprintf('Could not update row(s) from table %s', $table), (int)$exception->getCode(), $exception);
        }
    }

    /**
     * Replace rows from a database table
     *
     * @param string $table
     *          Table name
     * @param array  $data
     *          Values to insert
     * @param array  $where
     *          Replace condition
     */
    public function replace(string $table, array $data, array $where = []): void {
        try {
            $this->database->replace($table, $data, $where);
        } catch (Exception $exception) {
            throw new RuntimeException(sprintf('Could not replace row(s) from table %s', $table), (int)$exception->getCode(), $exception);
        }
    }

    /**
     * Fetch rows from a database table
     *
     * @param string $table
     *          Table name
     * @param array  $columns
     *          Columns to return
     * @param array  $where
     *          Select condition
     *
     * @return array
     */
    public function select(string $table, array $columns, array $where = []): array {
        try {
            return $this->database->select($table, $columns, $where);
        } catch (Exception $exception) {
            throw new RuntimeException(sprintf('Could not fetch row(s) from table %s', $table), (int)$exception->getCode(), $exception);
        }
    }

    /**
     * Execute a request on the database
     *
     * @param string $request
     *
     * @return array
     */
    public function query(string $request, array $data = []): void {
        try {
            $this->database->query($request, $data);
        } catch (Exception $exception) {
            throw new RuntimeException($exception->getMessage(), (int)$exception->getCode(), $exception);
        }
    }

    /**
     * Execute a request on the database
     *
     * @param string $request
     *
     * @return array
     */
    public function fetchQuery(string $request, array $data = []): array {
        try {
            return $this->database->query($request, $data)->fetchAll();
        } catch (Exception $exception) {
            throw new RuntimeException($exception->getMessage(), (int)$exception->getCode(), $exception);
        }
    }
}
