<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess;

use GuzzleHttp\Exception\GuzzleException;
use function GuzzleHttp\Psr7\build_query;
use JsonMapper_Exception;
use JsonMapper;
use WastedOnDestiny\Business\Cache\CacheStorageInterface;
use WastedOnDestiny\DataAccess\Models\BungieNetPlatformResponse;

class BungieNetPlatformContext extends GuzzleContext implements BungieNetPlatformContextInterface
{
    /** @var CacheStorageInterface */
    private $cacheStorage;

    public function __construct(CacheStorageInterface $cacheStorage, string $apiKey)
    {
        parent::__construct([ 'X-API-Key' => $apiKey ]);
        $this->cacheStorage = $cacheStorage;
    }

    /**
     * @param $method
     * @param $url
     * @param $parameters
     * @return BungieNetPlatformResponse
     * @throws GuzzleException
     */
    public function make(string $method, string $url, array $parameters = []): BungieNetPlatformResponse
    {
        if (!is_null($this->cacheStorage)) {
            $key = "{$method}_{$url}?" . build_query($parameters);
            $cachedEntry = $this->cacheStorage->fetch($key);

            if ($cachedEntry) {
                /** @var $cachedEntry BungieNetPlatformResponse */
                return $cachedEntry;
            }
        }

        $content = $this->call($method, $url, $parameters);

        $mapper = new JsonMapper();
        $mapper->bExceptionOnMissingData = true;

        if (!empty($content)) {
            try {
                $object = $mapper->map(json_decode($content, false), new BungieNetPlatformResponse());
                /** @var $object BungieNetPlatformResponse */
                
                if (!is_null($this->cacheStorage)) {
                    $this->cacheStorage->save($key, $object);
                }

                return $object;
            } catch (JsonMapper_Exception $e) { }
        }

        return new BungieNetPlatformResponse();
    }
}
