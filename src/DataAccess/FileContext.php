<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\DataAccess;

use Exception;

class FileContext implements FileContextInterface
{
    /**
     * @param $path
     * @param $parameters
     * @return array
     */
    public function make(string $path, array $parameters = []): array
    {
        try {
            $content = file_get_contents($path);

            if (!empty($content)) {
                return json_decode($content, true);
            }
        } catch (Exception $exception) {}

        return [];
    }
}
