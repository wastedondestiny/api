<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Cache;

use Exception;
use Predis\Client;

class RedisCacheStorage implements CacheStorageInterface {

    /** @var Client */
    private $cache;

    /** @var int */
    private $ttl;

    public function __construct($uri, $ttl)
    {
        $this->cache = new Client($uri);
        $this->ttl = $ttl;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function fetch(string $key)
    {
        try {
            if ($this->cache->exists($key)) {
                return unserialize($this->cache->get($key), ['allowed_classes' => true]);
            }
        } catch (Exception $exception) {
        }

        return null;
    }

    /**
     * @param string $key
     * @param mixed $data
     * @return bool
     */
    public function save(string $key, $data): bool
    {
        try {
            $this->cache->set($key, serialize($data));
            $this->cache->expire($key, $this->ttl);
            return true;
        } catch (Exception $exception) {
        }

        return false;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete(string $key): bool
    {
        try {
            if ($this->cache->exists($key)) {
                $this->cache->expire($key, 0);
                return true;
            }
        } catch (Exception $exception) {
        }

        return false;
    }

}
