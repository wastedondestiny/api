<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Cache;

use Exception;

class InMemoryCacheStorage implements CacheStorageInterface {

    /** @var array */
    private $cache = [];

    /**
     * @param string $key
     * @return mixed
     */
    public function fetch(string $key)
    {
        if (array_key_exists($key, $this->cache)) {
            try {
                return unserialize($this->cache[$key], ['allowed_classes' => true]);
            } catch (Exception $exception) {
                return null;
            }
        }

        return null;
    }

    /**
     * @param string $key
     * @param mixed $data
     * @return bool
     */
    public function save(string $key, $data): bool
    {
        $this->cache[$key] = serialize($data);
        return true;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete(string $key): bool
    {
        if (array_key_exists($key, $this->cache)) {
            unset($this->cache[$key]);
            return true;
        }

        return false;
    }

}
