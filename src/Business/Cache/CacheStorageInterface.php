<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Cache;

interface CacheStorageInterface {

    /**
     * @param string $key
     * @return mixed
     */
    public function fetch(string $key);

    /**
     * @param string $key
     * @param mixed $data
     * @return bool
     */
    public function save(string $key, $data): bool;

    /**
     * @param string $key
     * @return bool
     */
    public function delete(string $key): bool;

}
