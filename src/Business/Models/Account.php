<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Models;

use DateTime;

class Account
{
    /** @var DateTime */
    public $dateLastPlayed;

    /** @var int */
    public $grimoire;

    /** @var DestinyCharacterData[] */
    public $characters;
}