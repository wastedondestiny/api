<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Models;

class LeaderboardEntry
{
    /** @var string */
    public $membershipId;

    /** @var int */
    public $gameVersion;

    /** @var int */
    public $membershipType;

    /** @var int */
    public $timePlayed;

    /** @var string */
    public $displayName;
}