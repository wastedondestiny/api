<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Enums;

class Destiny2Season
{
    public const Undying = 3612906877;
    public const Dawn = 2007338097;
    public const Worthy = 4035491417;
    public const Arrivals = 248573323;
}
