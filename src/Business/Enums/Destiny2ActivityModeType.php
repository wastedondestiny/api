<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Enums;

class Destiny2ActivityModeType
{
    public const None = 0;
    public const Story = 2;
    public const Strike = 3;
    public const Raid = 4;
    public const AllPvP = 5;
    public const Patrol = 6;
    public const AllPvE = 7;
    public const Reserved9 = 9;
    public const Control = 10;
    public const Reserved11 = 11;
    public const Clash = 12;
    public const Reserved13 = 13;
    public const CrimsonDoubles = 15;
    public const Nightfall = 16;
    public const HeroicNightfall = 17;
    public const AllStrikes = 18;
    public const IronBanner = 19;
    public const Reserved20 = 20;
    public const Reserved21 = 21;
    public const Reserved22 = 22;
    public const Reserved24 = 24;
    public const AllMayhem = 25;
    public const Reserved26 = 26;
    public const Reserved27 = 27;
    public const Reserved28 = 28;
    public const Reserved29 = 29;
    public const Reserved30 = 30;
    public const Supremacy = 31;
    public const PrivateMatchesAll = 32;
    public const Survival = 37;
    public const Countdown = 38;
    public const TrialsOfTheNine = 39;
    public const Social = 40;
    public const TrialsCountdown = 41;
    public const TrialsSurvival = 42;
    public const IronBannerControl = 43;
    public const IronBannerClash = 44;
    public const IronBannerSupremacy = 45;
    public const ScoredNightfall = 46;
    public const ScoredHeroicNightfall = 47;
    public const Rumble = 48;
    public const AllDoubles = 49;
    public const Doubles = 50;
    public const PrivateMatchesClash = 51;
    public const PrivateMatchesControl = 52;
    public const PrivateMatchesSupremacy = 53;
    public const PrivateMatchesCountdown = 54;
    public const PrivateMatchesSurvival = 55;
    public const PrivateMatchesMayhem = 56;
    public const PrivateMatchesRumble = 57;
    public const HeroicAdventure = 58;
    public const Showdown = 59;
    public const Lockdown = 60;
    public const Scorched = 61;
    public const ScorchedTeam = 62;
    public const Gambit = 63;
    public const AllPvECompetitive = 64;
    public const Breakthrough = 65;
    public const BlackArmoryRun = 66;
    public const Salvage = 67;
    public const IronBannerSalvage = 68;
    public const PvPCompetitive = 69;
    public const PvPQuickplay = 70;
    public const ClashQuickplay = 71;
    public const ClashCompetitive = 72;
    public const ControlQuickplay = 73;
    public const ControlCompetitive = 74;
    public const GambitPrime = 75;
    public const Reckoning = 76;
    public const Menagerie = 77;
    public const VexOffensive = 78;
    public const NightmareHunt = 79;
    public const Elimination = 80;
    public const Momentum = 81;
    public const Dungeon = 82;
    public const Sundial = 83;
    public const TrialsOfOsiris = 84;
}
