<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Repositories\Leaderboard;

use WastedOnDestiny\Business\Models\LeaderboardEntry;

interface LeaderboardRepositoryInterface
{
    public function get(int $gameVersion, int $membershipType, int $page): array;
    public function count(?int $gameVersion = null, ?int $membershipType = null): int;
    public function getSingle(int $gameVersion, int $membershipType, string $membershipId): ?LeaderboardEntry;
    public function insert(int $gameVersion, int $membershipType, string $membershipId, int $timePlayed, string $displayName): void;
    public function upsert(int $gameVersion, int $membershipType, string $membershipId, int $timePlayed, string $displayName): void;
    public function update(int $gameVersion, int $membershipType, string $membershipId, int $timePlayed, string $displayName): void;
}
