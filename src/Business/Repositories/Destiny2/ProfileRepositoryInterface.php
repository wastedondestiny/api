<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

namespace WastedOnDestiny\Business\Repositories\Destiny2;

use WastedOnDestiny\Business\Models\Profile;

interface ProfileRepositoryInterface
{
    public function get(int $membershipType, string $membershipId, array $seasonPasses);
}
