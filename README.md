Time Wasted on Destiny
====================

## About
Time Wasted on Destiny is a website that tells you how much time you've spent on the Destiny series. On top of this, it is the only website that tells you just how much time you've spent on deleted characters, which you could consider wasted time.

**NOTE**: This page is only a documentation for https://api.wastedondestiny.com. If you're looking for information about the website itself, please go to https://gitlab.com/wastedondestiny/website.

## How to use
The API for Time Wasted on Destiny is really simple and easy to use.

> **https://api.wastedondestiny.com/accounts?membershipType=[]&membershipId=[]**
> Get all accounts associated with another.

> **https://api.wastedondestiny.com/accounts?displayName=[]**
> Get all accounts associated with a username.

> **https://api.wastedondestiny.com/search?displayName=[]**
> Autocomplete username from trials.report database (Thanks!)

> **https://api.wastedondestiny.com/characters?membershipType=[]&membershipId=[]&gameVersion=[]**
> Get characters and character information for an account.

> **https://api.wastedondestiny.com/activities?membershipType=[]&membershipId=[]&gameVersion=[]**
> Get last 30 days playtime for an account.

> **https://api.wastedondestiny.com/breakdown?membershipType=[]&membershipId=[]&characterId=[]&gameVersion=[]&page=[]**
> Get activity playtime breakdown for an account.

> **https://api.wastedondestiny.com/rank?membershipType=[]&membershipId=[]&gameVersion=[]**
> Get the account rank percentile based on other users searched on https://www.wastedondestiny.com.

> **https://api.wastedondestiny.com/leaderboard?membershipType=[]&gameVersion=[]&page=[]**
> Get pages of the leaderboard by game and platform

Keep in mind that this API is released free of charge and without subscription. Please do not abuse, or I will have to restrict access to the API.

## Installing
### Requirements
- Apache 2.4+
    - Include module
    - Rewrite module
- PHP 7.1+
    - JSON module
    - PDO module
- Composer

You should install the project root in your www/ folder, or a virtual host.
You have to install dependencies with `composer install`.
Don't forget to make a copy of `config.example.php` and supply your own API key.

## Disclaimer
Time Wasted on Destiny is not affiliated with Bungie. Destiny is a registered trademark of Bungie and Activision.

## Licence
This project is distributed under the GPL v3.0 license. Please give credit if you intend on using it!
