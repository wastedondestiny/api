<?php
class GlobalAlertsRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testGivenEmptyBungieNetPlatformResponseWhenGetThenEmptyArray()
    {
        $json = '{"Response":[],"ErrorCode":1,"ThrottleSeconds":0,"ErrorStatus":"Success","Message":"Ok","MessageData":{}}';
        $mapper = new JsonMapper();
        $response = $mapper->map(json_decode($json), new \WastedOnDestiny\DataAccess\Models\BungieNetPlatformResponse);
        $context = \Codeception\Stub::makeEmpty(\WastedOnDestiny\DataAccess\BungieNetPlatformContextInterface::class, [
            'make' => $response
        ]);
        $repo = new \WastedOnDestiny\DataAccess\Repositories\Core\GlobalAlertsRepository($context);
        $result = $repo->get();

        $this->assertEmpty($result);
        $this->assertCount(0, $result);
    }

    public function testGivenBungieNetPlatformResponseWhenGetThenGlobalAlert()
    {
        $json = '{"Response":[{"alertHtml":"<h1>Warning!</h1>","alertTimestamp":"2017-11-01T03:36:30Z",
                "alertLink":"https://www.wastedondestiny.com","alertLevel":1,"alertType":1}],"ErrorCode":1,
                "ThrottleSeconds":0,"ErrorStatus":"Success","Message":"Ok","MessageData":{}}';
        $mapper = new JsonMapper();
        $response = $mapper->map(json_decode($json), new \WastedOnDestiny\DataAccess\Models\BungieNetPlatformResponse);
        $context = \Codeception\Stub::makeEmpty(\WastedOnDestiny\DataAccess\BungieNetPlatformContextInterface::class, [
            'make' => $response
        ]);
        $repo = new \WastedOnDestiny\DataAccess\Repositories\Core\GlobalAlertsRepository($context);
        $result = $repo->get();

        $this->assertNotEmpty($result);
        $this->assertCount(1, $result);
        $this->assertInstanceOf(\WastedOnDestiny\Business\Models\GlobalAlert::class, $result[0]);
        $this->assertNotEquals($result[0], $response->response);
        $this->assertEquals($result[0]->html, $response->response[0]->alertHtml);
        $this->assertEquals($result[0]->timestamp, $response->response[0]->alertTimestamp);
        $this->assertEquals($result[0]->link, $response->response[0]->alertLink);
        $this->assertEquals($result[0]->level, $response->response[0]->alertLevel);
        $this->assertEquals($result[0]->type, $response->response[0]->alertType);
    }
}