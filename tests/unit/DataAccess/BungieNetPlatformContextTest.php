<?php 
class BungieNetPlatformContextTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testGivenRequestWhenMakeThenBungieNetPlatformResponse()
    {
        $headers = ['Content-Type' => 'application/json'];
        $body = new \WastedOnDestiny\DataAccess\Models\BungieNetPlatformResponse();
        $body->response = 'Test';

        $response = new \GuzzleHttp\Psr7\Response(200, $headers, json_encode($body));
        $mock = new \GuzzleHttp\Handler\MockHandler([ $response ]);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $client = new \GuzzleHttp\Client([ 'handler' => $handler ]);

        $context = \Codeception\Stub::makeEmptyExcept(
            \WastedOnDestiny\DataAccess\BungieNetPlatformContext::class,
            'make',
            [
                'apiKey' => '',
                'cachePath' => codecept_output_dir(),
                'client' => $client
            ]
        );
        $response = $context->make('GET', 'https://www.wastedondestiny.com');

        $this->assertInstanceOf(\WastedOnDestiny\DataAccess\Models\BungieNetPlatformResponse::class, $response);
        $this->assertEquals('Test', $response->response);
    }

    public function testGivenEmptyResponseWhenMakeThenBungieNetPlatformResponse()
    {
        $headers = ['Content-Type' => 'application/json'];
        $response = new \GuzzleHttp\Psr7\Response(200, $headers, '');
        $mock = new \GuzzleHttp\Handler\MockHandler([ $response ]);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $client = new \GuzzleHttp\Client([ 'handler' => $handler ]);

        $context = \Codeception\Stub::makeEmptyExcept(
            \WastedOnDestiny\DataAccess\BungieNetPlatformContext::class,
            'make',
            [
                'apiKey' => '',
                'cachePath' => codecept_output_dir(),
                'client' => $client
            ]
        );
        $response = $context->make('GET', 'https://www.wastedondestiny.com');

        $this->assertInstanceOf(\WastedOnDestiny\DataAccess\Models\BungieNetPlatformResponse::class, $response);
        $this->assertEmpty($response->response);
    }

    public function testGiven404WhenMakeThenGuzzleHttpException()
    {
        $this->expectException(\GuzzleHttp\Exception\GuzzleException::class);

        $headers = ['Content-Type' => 'application/json'];
        $response = new \GuzzleHttp\Psr7\Response(404, $headers, '');
        $mock = new \GuzzleHttp\Handler\MockHandler([ $response ]);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $client = new \GuzzleHttp\Client([ 'handler' => $handler ]);

        $context = \Codeception\Stub::makeEmptyExcept(
            \WastedOnDestiny\DataAccess\BungieNetPlatformContext::class,
            'make',
            [
                'apiKey' => '',
                'cachePath' => codecept_output_dir(),
                'client' => $client
            ]
        );
        $context->make('GET', 'https://www.wastedondestiny.com');
    }
}