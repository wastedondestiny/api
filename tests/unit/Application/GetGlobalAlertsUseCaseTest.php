<?php

use WastedOnDestiny\Application\UseCases\GetGlobalAlertsUseCase;

class GetGlobalAlertsUseCaseTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testGivenNoGlobalAlertWhenExecuteThenEmptyArray()
    {
        $globalAlertsRepository = \Codeception\Stub::makeEmpty(\WastedOnDestiny\Business\Repositories\Core\GlobalAlertsRepositoryInterface::class, [
            'get' => []
        ]);
        $useCase = new GetGlobalAlertsUseCase($globalAlertsRepository);
        $result = $useCase->execute();

        $this->assertEmpty($result);
        $this->assertCount(0, $result);
    }

    public function testGivenGlobalAlertWhenExecuteThenGlobalAlert()
    {
        $ga = new \WastedOnDestiny\Business\Models\GlobalAlert();
        $ga->type = 1;
        $ga->level = 1;
        $ga->link = 'https://www.wastedondestiny.com';
        $ga->timestamp = '2017-11-01T03:36:30Z';
        $ga->html = '<h1>Warning!</h1>';
        $globalAlertsRepository = \Codeception\Stub::makeEmpty(\WastedOnDestiny\Business\Repositories\Core\GlobalAlertsRepositoryInterface::class, [
            'get' => [$ga]
        ]);
        $useCase = new GetGlobalAlertsUseCase($globalAlertsRepository);
        $result = $useCase->execute();

        $this->assertNotEmpty($result);
        $this->assertCount(1, $result);
        $this->assertNotEquals($result[0], $ga);
        $this->assertInstanceOf(\WastedOnDestiny\Application\Models\GlobalAlert::class, $result[0]);
        $this->assertEquals($result[0]->html, $ga->html);
        $this->assertEquals($result[0]->timestamp, $ga->timestamp);
        $this->assertEquals($result[0]->link, $ga->link);
        $this->assertEquals($result[0]->level, $ga->level);
        $this->assertEquals($result[0]->type, $ga->type);
    }
}