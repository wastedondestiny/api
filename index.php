<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

require './vendor/autoload.php';

use WastedOnDestiny\Api\App;
use WastedOnDestiny\Api\Controllers\AccountsController;
use WastedOnDestiny\Api\Controllers\ActivitiesController;
use WastedOnDestiny\Api\Controllers\BreakdownController;
use WastedOnDestiny\Api\Controllers\CharactersController;
use WastedOnDestiny\Api\Controllers\LeaderboardController;
use WastedOnDestiny\Api\Controllers\RankController;
use WastedOnDestiny\Api\Controllers\SearchController;
use WastedOnDestiny\Api\Controllers\StatusController;
use WastedOnDestiny\Api\Middlewares\JsonContentMiddleware;
use WastedOnDestiny\Api\Middlewares\CacheMiddleware;

set_error_handler(static function ($severity, $message, $file, $line) {
    if (!in_array($severity, array(E_USER_ERROR, E_RECOVERABLE_ERROR), true)) {
        return;
    }

    throw new ErrorException($message, 500, $severity, $file, $line);
});

$app = new App(require './config.php');
$app->add(CacheMiddleware::class);
$app->add(JsonContentMiddleware::class);

$app->get('/status', StatusController::class);
$app->get('/accounts', AccountsController::class);
$app->get('/search', SearchController::class);
$app->get('/characters', CharactersController::class);
$app->get('/activities', ActivitiesController::class);
$app->get('/breakdown', BreakdownController::class);
$app->get('/leaderboard', LeaderboardController::class);
$app->get('/rank', RankController::class);

try {
    $app->run();
} catch (Exception $e) {
    die($e->getMessage());
}
