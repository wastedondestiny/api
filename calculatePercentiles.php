<?php
/**
 * Time Wasted on Destiny
 *
 * @link      https://gitlab.com/binarmorker/TimeWastedOnDestiny
 * @copyright Copyright (c) 2018 François Allard, Tommy Teasdale
 * @license   https://gitlab.com/binarmorker/TimeWastedOnDestiny/blob/master/LICENSE (GNU GPLv3)
 */
declare(strict_types=1);

define('DESTINY', 1);
define('DESTINY2', 2);
define('XBOX', 1);
define('PLAYSTATION', 2);
define('STEAM', 3);
define('STADIA', 5);
define('UNKNOWN', -1);

require './vendor/autoload.php';

$config = require './config.php';

$database = new \Medoo\Medoo([
    'database_type' => $config['database']['type'],
    'database_name' => $config['database']['name'],
    'server'        => $config['database']['host'],
    'username'      => $config['database']['user'],
    'password'      => $config['database']['password'],
    'charset'       => $config['database']['charset'],
    'port'          => $config['database']['port'],
    'option'        => [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]
]);

$context = new \WastedOnDestiny\DataAccess\DatabaseContext($database);

function calculatePercentiles(\WastedOnDestiny\DataAccess\DatabaseContext $context, int $gameVersion, int $membershipType) {
    if ($membershipType > 0) {
        $results = $context->fetchQuery("SELECT MIN(`d`.`timePlayed`) as `timePlayed`, `d`.`percentileRank` + 1 as `percentileRank`
                                        FROM (SELECT `c`.`timePlayed`, ROUND(((@rank - `rank`) / @rank) * 99, 0) AS `percentileRank`
                                        FROM (SELECT *, @prev:=@curr, @curr:=`a`.`timePlayed`, @rank:=IF(@prev = @curr, @rank, @rank + 1) AS `rank`
                                        FROM (SELECT `membershipId`, `timePlayed` FROM `leaderboard` 
                                        WHERE `membershipType` = $membershipType AND `gameVersion` = $gameVersion
                                        ) AS `a`, (SELECT @curr:=null, @prev:=null, @rank:=0) AS `b` ORDER BY `timePlayed`) AS `c`) AS `d` GROUP BY `d`.`percentileRank`");
    } else {
        $results = $context->fetchQuery("SELECT MIN(`d`.`timePlayed`) as `timePlayed`, `d`.`percentileRank` + 1 as `percentileRank`
                                        FROM (SELECT `c`.`timePlayed`, ROUND(((@rank - `rank`) / @rank) * 99, 0) AS `percentileRank`
                                        FROM (SELECT *, @prev:=@curr, @curr:=`a`.`timePlayed`, @rank:=IF(@prev = @curr, @rank, @rank + 1) AS `rank`
                                        FROM (SELECT `membershipId`, `timePlayed` FROM `leaderboard` 
                                        WHERE `gameVersion` = $gameVersion
                                        ) AS `a`, (SELECT @curr:=null, @prev:=null, @rank:=0) AS `b` ORDER BY `timePlayed`) AS `c`) AS `d` GROUP BY `d`.`percentileRank`");
    }

    foreach ($results as $result) {
        $existing = $context->select('percentiles', [
            'timePlayed'
        ], [
            'gameVersion' => $gameVersion,
            'membershipType' => $membershipType,
            'rank' => $result['percentileRank']
        ]);

        if (\count($existing) === 0) {
            $context->insert('percentiles', [
                'gameVersion' => $gameVersion,
                'membershipType' => $membershipType,
                'rank' => $result['percentileRank'],
                'timePlayed' => $result['timePlayed']
            ]);
        } else {
            $context->update('percentiles', [
                'timePlayed' => $result['timePlayed']
            ], [
                'gameVersion' => $gameVersion,
                'membershipType' => $membershipType,
                'rank' => $result['percentileRank']
            ]);
        }
    }
}

calculatePercentiles($context, DESTINY, XBOX);
calculatePercentiles($context, DESTINY, PLAYSTATION);
calculatePercentiles($context, DESTINY2, XBOX);
calculatePercentiles($context, DESTINY2, PLAYSTATION);
calculatePercentiles($context, DESTINY2, STEAM);
calculatePercentiles($context, DESTINY2, STADIA);
calculatePercentiles($context, DESTINY2, UNKNOWN);
